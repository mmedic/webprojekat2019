using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Hometel.Domain.Models;
using Hometel.Domain.Services;
using Hometel.Domain.Models.Dto;
using AutoMapper;

namespace Hometel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HostController : ControllerBase
    {
        private readonly IApartmentService _apartmentService;
        private readonly IMapper _mapper;
        public HostController(IApartmentService apartmentService, IMapper mapper){
            _apartmentService = apartmentService; 
            _mapper = mapper;
        }

        [Authorize(Roles = Role.Host)]
        [HttpPost("add_apartment")]
        public async Task<IActionResult>  AddApartment([FromBody] ApartmentDto apartmentDto) {
            // Steps:
            // ApartmentDto arrives from frontend
            // Check if Model is valid
            if(!ModelState.IsValid){
                return BadRequest(ModelState);
            }
            // Mapped into Apartment Model
            var apartment = _mapper.Map<Apartment>(apartmentDto);
            // Check if Apartment already exists in users list of apartments
            var foundApartment = await _apartmentService.FindApartmentAsync(apartment.Id);
            if(foundApartment != null){
                return BadRequest("Apartment already exists");
            }
            // Create the apartment
            try{
                var added = await _apartmentService.CreateApartmentAsync(apartment);
                if(added == null){
                    return BadRequest("There was an error while creatign the appartment");
                }
                return Ok(added);
            }catch(AppException ex){
                return BadRequest("There was an error while creating the project " + new {message = ex.Message});
            }
            // Return apartment
            // Add apartment to users list of apartments
            // Return the apartment
        }
        [Authorize(Roles = Role.Host)]
        [HttpDelete("delete_apartment/{id}")]
        public async Task<IActionResult> DeleteApartment([FromRoute] int id){
            //check if id != null
            if(id == null){
                return BadRequest("The apartment id is null");
            }
            //check if apartment exists
            var exists = await _apartmentService.FindApartmentAsync(id);
            if(exists == null){
                return BadRequest("The apartment you want to delete doesn't exist");
            }
            //if apartment exists, delete apartment
            var deleted = await _apartmentService.DeleteApartmentAsync(id);
            if(!deleted){
                return BadRequest("There was an error while deleting the apartment");
            }
            //else return apartment not found
            return Ok(204);
        }
    }
}
