using System.Collections.Generic;
using System;
using Hometel.Domain.Models;
using System.Threading.Tasks;
using Hometel.Domain.Services;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Hometel.Domain.Repositories;
using System.Text;
using Microsoft.Extensions.Options;
using Hometel.Domain.Models.Dto;

namespace Hometel.Services {
    public class ApartmentService : IApartmentService {
        private readonly IApartmentRepository _apartmentRepository;
        public ApartmentService(IApartmentRepository apartmentRepository) {
            _apartmentRepository = apartmentRepository;
        }
        public async Task<Apartment> CreateApartmentAsync(Apartment apartment){
            try {
                await _apartmentRepository.CreateAparment(apartment);
            }catch(AppException ex){
                Console.WriteLine(new {message = ex.Message});
            }
             return await _apartmentRepository.FindApartment(apartment.Id);
        }
        public async Task<Apartment> FindApartmentAsync(int id){
            return await _apartmentRepository.FindApartment(id);
        }
        public async Task<bool> DeleteApartmentAsync(int id){
            try{
                await _apartmentRepository.DeleteApartment(id);
                return true;
            } catch(Exception ex){
                Console.WriteLine(new {message = ex.Message});
                return false;
            }
        }
    }
}