using System.Collections.Generic;
using Hometel.Domain.Models;
using System.Threading.Tasks;
using Hometel.Domain.Models.Dto;

namespace Hometel.Domain.Services {
    public interface IApartmentService {
        Task<Apartment> CreateApartmentAsync(Apartment apartment);
        Task<Apartment> FindApartmentAsync(int id);
        Task<bool> DeleteApartmentAsync(int id);
    }
}