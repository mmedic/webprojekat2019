using System.ComponentModel.DataAnnotations.Schema;

namespace Hometel.Domain.Models {
    public class Location {
        public int Id {get; set;}
        public float Latitude {get; set;}
        public float Longitude {get; set;}
        //[ForeignKey("Address")]
        //public int AddressId {get; set;}
        public virtual Address Address {get; set;} //format: Street StreetNum, Town TownPostalNum
        //public int ApartmentId {get; set;}
        //public virtual Apartment Apartment {get; set;}
    }
}