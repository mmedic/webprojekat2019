using System;
namespace Hometel.Domain.Models.Helpers {
    public class AvailableDatesForRent { //dates which have not been taken
        public int Id {get; set;}
        public DateTime AvailableDate {get; set;}
    }
}