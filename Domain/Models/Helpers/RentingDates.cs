using System;
namespace Hometel.Domain.Models.Helpers {
    public class RentingDates { //all dates for renting
        public int Id {get; set;}
        public DateTime ApartmentDate {get; set;}
    }
}