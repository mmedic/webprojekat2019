using System.Collections.Generic;
using Hometel.Domain.Models.Helpers;
using System.ComponentModel.DataAnnotations;
namespace Hometel.Domain.Models.Dto
{
    public class ApartmentDto
    {
        [Required]
        public EApartmentType ApartmentType {get; set;}
        [Required]
        public int RoomNumber {get; set;}
        [Required]
        public int GuestNumber {get; set;}
        [Required]
        public Location Location {get; set;}
        [Required]
        public IList<RentingDates> Dates {get; set;} = new List<RentingDates>(); //DbContext se buni kada ga stavim kao DateTime
        [Required]
        public IList<AvailableDatesForRent> AvailableDates {get; set;} = new List<AvailableDatesForRent>(); //DbContext se buni kada ga stavim kao DateTime, menja se prilikom svake rezervacije etc...
        [Required]
        public string HostId {get; set;}
        [Required]
        public int Price {get; set;}
        public int ReservationStartTime {get; set;}
        public int ReservationEndTime {get; set;}
        public EApartmentStatus AppartmentStatus {get; set;}
        [Required]
        public IList<Amenity> Amenities {get; set;} = new List<Amenity>();
    }
}