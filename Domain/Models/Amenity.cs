namespace Hometel.Domain.Models {
    public class Amenity {
        public int Id {get; set;}
        public string Name {get; set;}
        public int? ApartmentId { get; set; }
        public Apartment Apartment {get; set;}
    }
}