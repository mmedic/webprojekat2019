using System.Collections.Generic;
using System.Threading.Tasks;
using Hometel.Domain.Models;

namespace Hometel.Domain.Repositories {
    public interface IApartmentRepository {
        Task CreateAparment(Apartment apartment);
        Task<Apartment> FindApartment(int id);
        Task DeleteApartment(int id);
    }
}