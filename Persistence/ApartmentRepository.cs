using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hometel.Domain.Repositories;
using Hometel.Domain.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Hometel.Persistence {
    public class ApartmentRepository : BaseRepository, IApartmentRepository {

        public ApartmentRepository(AppDbContext context) : base(context) {
        }
        public async Task CreateAparment(Apartment apartment){
            await _context.Apartments.AddAsync(apartment);
            _context.SaveChanges();
        }
        public async Task<Apartment> FindApartment(int id){
            return await _context.Apartments.FindAsync(id);
        }
        public async Task DeleteApartment(int id){
            var apartment = await _context.Apartments.FindAsync(id);
            Console.WriteLine(apartment);
            var location = await _context.Locations.FindAsync(apartment.Location.Id);
            if(location != null){
                _context.Locations.Remove(location);
            }
            //delete renting dates
            foreach(var apartmentRentingDate in apartment.Dates){
                var rentDate = await _context.RentingDates.FindAsync(apartmentRentingDate.Id);
                if(rentDate != null){
                    _context.RentingDates.Remove(rentDate);
                }
            }
            //delete renting dates
            foreach(var apartmentAvailableDate in apartment.Dates){
                var availableDate = await _context.AvailableDatesForRent.FindAsync(apartmentAvailableDate.Id);
                if(availableDate != null){
                    _context.AvailableDatesForRent.Remove(availableDate);
                }
            }
            _context.Apartments.Remove(apartment);
            _context.SaveChanges();
            /*            var host = await _context.Hosts.FindAsync(apartment.HostId);
            var obj = host.ListOfApartments.Where(w => w.Id == apartment.Id).FirstOrDefault();
            if(obj != null){
                host.ListOfApartments.Remove(obj);
                _context.Apartments.Remove(apartment);
            }
            _context.SaveChanges();
 */
        }
    }
}